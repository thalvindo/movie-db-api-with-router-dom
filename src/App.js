import {Routes, Route} from 'react-router-dom';

import './App.css';
import MovieList from './Components/MovieList/MovieList';
import GenreTable from './Components/GenreTable/GenreTable';
import MovieDetail from './Components/MovieDetail/MovieDetail';

const App = () => {
  return (
    <Routes>
      <Route path="/" element={<MovieList />} />
      <Route path="/genre" element={<GenreTable />} />
      <Route path="/movie/:id" element={<MovieDetail />}></Route>
    </Routes>
  );
};

export default App;
