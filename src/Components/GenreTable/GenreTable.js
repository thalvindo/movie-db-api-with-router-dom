import React, {useEffect} from 'react';
import axios from "axios";
import { useDispatch, useSelector } from 'react-redux';
import { Link } from "react-router-dom";

import { addGenreList } from '../../Redux/blank';
import './GenreTable.css';

const GenreTable = () => {
    const dispatch = useDispatch();
    const { genreList } = useSelector(state => state.blank);

    useEffect(() => {
        const fetchData = async () => {
            const {data} = await axios.get('https://api.themoviedb.org/3/genre/movie/list?api_key=2fccde01a371b106b09a241d6d1d5b49');
            dispatch(addGenreList(data.genres));
        }
        fetchData();
    }, [])

    return (
        <div className="genre-table">
            <h1>List of Genres</h1>
            <div>
                <Link className="back"to="/">Back to Dashboard</Link>
            </div>
            <table className="center" >
                <tr>
                    <th>ID</th>
                    <th>Genre Name</th>
                </tr>
                {genreList.map((genre, key) => {
                    return (
                        <tr>
                            <th>
                                {genre.id}
                            </th>
                            <th>
                                {genre.name}
                            </th>
                        </tr>
                    );
                })}
            </table>
        </div>
    )
}

export default GenreTable;