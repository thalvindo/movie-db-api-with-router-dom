import React, {useEffect, useState} from 'react';
import { useSelector } from 'react-redux';
import axios from 'axios';
import {
    useParams,
    Link
  } from "react-router-dom";

import constants from '../../Constants/Constants';
import './MovieDetail.css';

const MovieDetail = () => {
    const { genreList } = useSelector(state => state.blank);
    const [movie, setMovie] = useState();
    const { id } = useParams();

    useEffect(() => {
        const getData = async () => {
            const {data} = await axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=2fccde01a371b106b09a241d6d1d5b49`);
            setMovie(data);
        }
        getData();
    }, [])

    const renderGenres = () => {
        let arrayGenres = [];
        let stringGenres = '';
        genreList.map((genre) => {
            movie.genres.map((value) => {
                if(genre.id === value.id) {
                    arrayGenres.push(genre.name);
                }
            })
        })
        stringGenres = (arrayGenres.toString());
        stringGenres = stringGenres.replace(/,/g, ', ');

        return (
            <>
                <h4 className="genre-movie">{stringGenres}</h4>
            </>
        )
    }
    
    return (
        <div className="overlay">
            <>
                <Link className="back"to="/">Back to Dashboard</Link>
            </>
            { movie !== undefined ?
                <div className="modal-container">
                <h3 className="movie-title-modal">{movie.title}</h3>
                <img src={constants.baseURL + movie.poster_path}/>
                <div className="genre-name">
                    <h3 className="genre">Genre: </h3>
                    {
                        renderGenres()
                    }
                </div>
                <h3>Popularity: {movie.popularity}</h3>
                <h3>Vote Count: {movie.vote_count}</h3>
                <h3>Vote Average: {movie.vote_average}</h3>
                <h3>Release: {movie.release_date}</h3>
                <h3>Language: {movie.original_language}</h3>
                <div className="movie-overview">
                    {movie.overview ? 
                        <h4 className="movie-overview-detail">{movie.overview}</h4>
                    :
                        <h4 className="no-overview">no overview for this movie</h4>
                    }
                </div>
            </div> 
                : <></>
            }

        </div>
    )
};

export default MovieDetail;