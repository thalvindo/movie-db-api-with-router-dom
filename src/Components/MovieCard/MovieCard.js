import React from 'react';
import { Link } from 'react-router-dom';

import './MovieCard.css';
import constants from '../../Constants/Constants';

const MovieCard = ({data}) => {
    return (
        <div className="movie-card-container">
            <Link to={`/movie/${data.id}`} params={data}>
                <div className="movie-card">
                    <h3 className="movie-title">{data.title}</h3>
                    <img src={constants.baseURL + data.poster_path} />
                </div>
            </Link>
        </div>
    )
};

export default MovieCard;